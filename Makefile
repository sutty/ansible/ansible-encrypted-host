hosts ?=

.DEFAULT_GOAL := help

help: ## Ayuda
	@echo "Instalar: make install hosts=el.nombre.de.dominio.completo"
	@echo "Desbloquear: make unblock"

install: ## Instala (argumento: hosts)
	@echo "Comprobando argumento hosts"
	@test -n "$(hosts)"
	ansible-playbook --ask-vault-pass -e hosts=$(hosts) encrypted-host.yml

unblock: ## Desbloquea
	ansible-playbook --ask-vault-pass unblock.yml
